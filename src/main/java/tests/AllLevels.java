package tests;
import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;

public class AllLevels {

	private static WebDriver driver = null;
	private By startButton = By.id("start_button");
	private By levelTitle = By.cssSelector("h1");

	private By inputId = By.id("input");
	private By buttonId = By.id("next");
	
	private String pageUrl = "http://pruebaselenium.serviciosdetesting.com/";
	
	private static Logger logger = Logger.getAnonymousLogger();
	
	private enum Title {
		LEVEL1("Práctica Selenium"),
		LEVEL2("Level 2"),
		LEVEL3("Level 3"),
		LEVEL4("Level 4"),
		LEVEL5("Level 5"),
		LEVEL6("Level 6"),
		LEVEL7("Level 7"),
		LEVEL8("Level 8"),
		LEVEL9("Level 9"),
		LEVEL10("Level 10");
		
		private String level;
		
		Title(String level) {
			this.level = level;
		}
		
		public String getText() {
			return level;
		}
	}

	@BeforeClass
	public static void setUpBeforeClass() {
		try {
			ArrayList<String> optionsList = new ArrayList<String>();
			ChromeOptions chromeOptions = new ChromeOptions();
			optionsList.add("--start-maximized");
			optionsList.add("--incognito");
			optionsList.add("disable-notifications");
			chromeOptions.addArguments(optionsList);
			chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
	  		chromeOptions.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
			System.setProperty("webdriver.chrome.driver",
	                "chromedriver");

			driver = new ChromeDriver(chromeOptions);

			throw new UnsupportedOperationException(
					"unsupported operation for setting up chrome driver.");

		} catch (UnsupportedOperationException exception) {
			logger.log(Level.SEVERE, "an exception was thrown", exception);
		}
	}

	@AfterClass
	public static void tearDownAfterClass() {
		try {
			//Close browser
			driver.close();
			throw new UnsupportedOperationException("error while closing browser");
		} catch (UnsupportedOperationException exception) {
			logger.log(Level.SEVERE, "an exception was thrown", exception);
		}
	}

	@Before
	public void setUp() {
		// for this example setup needed is implemented in @BeforeClass
	}

	@After
	public void tearDown() {
		// for this example tearDown needed is implemented in @AfterClass
	}

	@Test
	public void test() {
		//Open URL
		driver.get(pageUrl);
		WebElement levelTitleElement = driver.findElement(levelTitle);
		
		//Level 1
		assertEquals(levelTitleElement.getText(), Title.LEVEL1.getText());

		WebElement startButtonElement = driver.findElement(startButton);
		startButtonElement.click();
		levelTitleElement = driver.findElement(levelTitle);

		assertEquals(levelTitleElement.getText(), Title.LEVEL2.getText());

		//Level 2
		driver.findElement(inputId).sendKeys("selenium");
		driver.findElement(buttonId).click();
		levelTitleElement = driver.findElement(levelTitle);

		assertEquals(levelTitleElement.getText(), Title.LEVEL3.getText());

		//Level 3
		driver.findElement(inputId).sendKeys("I am doing Selenium stuff!");
		driver.findElement(buttonId).click();
		levelTitleElement = driver.findElement(levelTitle);

		assertEquals(levelTitleElement.getText(), Title.LEVEL4.getText());

		//Level 4
		List<WebElement> buttonsLevel4 = driver.findElements(By.className("btn-lga"));
		for(WebElement button : buttonsLevel4) {
			button.click();
		}
		levelTitleElement = driver.findElement(levelTitle);

		assertEquals(levelTitleElement.getText(), Title.LEVEL5.getText());

		//Level 5
		WebElement linkLevel5 = driver.findElement(By.linkText("Enlace!"));
		linkLevel5.click();
		levelTitleElement = driver.findElement(levelTitle);

		assertEquals(levelTitleElement.getText(), Title.LEVEL6.getText());
		
		//Level 6
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(
			"document.getElementById('hidden\"').setAttribute('style', 'display:inline;')"
		);
		driver.findElement(By.id("hidden\"")).click();
		
		//Level 7
		driver.switchTo().alert().accept();

		//Level 8
		Alert alertLevel8 = driver.switchTo().alert();
		alertLevel8.sendKeys("9");
		alertLevel8.accept();
		driver.switchTo().window(driver.getWindowHandle());
		levelTitleElement = driver.findElement(levelTitle);

		assertEquals(levelTitleElement.getText(), Title.LEVEL9.getText());
		
		//Level 9
		Set<String> setHandles = driver.getWindowHandles();
		String[] windows = setHandles.toArray(new String[setHandles.size()]);
		
		driver.switchTo().window(windows[1]);
		String password = driver.findElement(By.id("pass")).getText();
		driver.close();
		driver.switchTo().window(windows[0]);
		driver.findElement(inputId).sendKeys(password);
		driver.findElement(buttonId).click();
		levelTitleElement = driver.findElement(levelTitle);

		assertEquals(levelTitleElement.getText(), Title.LEVEL10.getText());
		
		//Level 10
		WebElement from = driver.findElement(By.id("source"));
		WebElement to = driver.findElement(By.id("target"));

		// Actions class method to drag and drop
		Actions action = new Actions(driver);
		// Perform drag and drop
		action.dragAndDrop(from, to).build().perform();

		levelTitleElement = driver.findElement(levelTitle);
		assertEquals(levelTitleElement.getText(),
				"¡Enhorabuena! Has llegado al final de la práctica");
	}
}
